# Test
Node version 16.6.0

## Clone Project

```bash
git clone {repo url}
cd test
```


## Environment Variables

Environment Variables
To run this project, you will need to add the following environment variables to your .env file

PORT

WIA_TOKEN

## Installation

Install with npm

```bash
  npm install
  npm run dev
```
## API Documentation 

```bash
    http://localhost:{PORT}/swagger/
```
http://localhost:3000/swagger/

## API Reference

#### Get all spaces

```http
  GET /api/test/spaces
```


#### Get all devices

```http
  GET /api/test/devices/${spaceId}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `spaceId`      | `string` | **Required** |


#### Get all devices

```http
  GET /api/test/events/${deviceId}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `deviceId`      | `string` | **Required** |


#### Get csv Link and events

```http
  GET /api/test/csv
```

## Tests

Run Tests using
```
 npm test
```