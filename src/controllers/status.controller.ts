import { Controller, Get, Route, Tags } from 'tsoa';
import { Response, Request } from 'express'
import { IResponse } from '../interfaces';
import { fullUrl, successResponse } from '../utils';

@Tags('Status')
@Route('api')
export default class StatusController extends Controller {
    req: Request;
    res: Response;
    constructor(req: Request, res: Response) {
        super();
        this.req = req;
        this.res = res;
    }
    @Get('/status')
    public async status(): Promise<IResponse> {
        return successResponse({
            fullUrl
        })
    }
}