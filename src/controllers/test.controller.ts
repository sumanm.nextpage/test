import { Controller, Get, Query, Route, Tags } from 'tsoa';
import { Response, Request } from 'express';
import { getCSV, getDevices, getEvents, getSpaces } from '../services';
import { successResponse } from '../utils';
import { IResponse } from '../interfaces';

@Tags('Test')
@Route('api/test')
export default class TestController extends Controller {
    req: Request;
    res: Response;
    constructor(req: Request, res: Response) {
        super();
        this.req = req;
        this.res = res;
    }
    @Get('/spaces')
    public async spaces(): Promise<IResponse> {
        const getSpacesRes = await getSpaces();
        return successResponse(getSpacesRes);
    }
    @Get('/devices')
    public async devices(@Query() spaceId: string): Promise<IResponse> {
        const getDevicesRes = await getDevices(spaceId);
        return successResponse(getDevicesRes);
    }
    @Get('/events')
    public async events(@Query() deviceId: string): Promise<IResponse> {
        const getDevicesRes = await getEvents(deviceId);
        return successResponse(getDevicesRes);
    }
    @Get('/csv')
    public async csv(): Promise<IResponse> {
        const getDevicesRes = await getCSV();
        return successResponse(getDevicesRes);
    }
}