import bcrypt from 'bcryptjs';
import fs from "fs"
import moment from 'moment';
import sharp from 'sharp'
import path from 'path'
import vision from '@google-cloud/vision'
import logger from '../config/logger';
import { Types } from 'mongoose';

const visionClient = new vision.ImageAnnotatorClient({
    keyFile: path.join(__dirname, '../', 'config', 'cloud.json')
})

const accountNumberRegex = /^\d{3}-\d{3}-\d{1}-\d{6}-\d{1}$/
const accountNumberRegexBrackets = /\(\d{3}-\d{3}-\d{1}-\d{6}-\d{1}\)/g
const chineseChars = /[\u4E00-\u9FFF\u3400-\u4DFF\uF900-\uFAFF]+/

export const isDateFormat = (data: string) => {
    const dateRE = /^\d{4}\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/
    return dateRE.test(data);
}

export const isDateFormatDash = (data: string) => {
    const dateRE = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/
    return dateRE.test(data);
}

export const isAmount = (data: string) => {
    return data.includes('.') && !data.includes('-') && !(accountNumberRegex.test(data)) && !chineseChars.test(data);
}

export const isAccountNumber = (data: string) => {
    return data.includes('-') && (accountNumberRegex.test(data)) && !chineseChars.test(data) && data.length > 10;
}

export const isAccountNumberWithBrackets = (data: string) => {
    return accountNumberRegexBrackets.test(data);
}

export const getDateByRegx = (parsedString: string) => {
    const regs = [
        /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
        /([012]?\d)[ \/.-]([0123]?\d)[ \/.-]([012]\d{4})\b/,
        /([012]?\d)[ \/.-]([0123]?\d)[ \/.-]([012]\d{1})\b/,
        /\d{1,2}\s\w{3,5}\s\d{2,4}/,
        /\d{2}\s\w+\s\d{2,4}/,
        /(([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4}/,
        /(?:(?:(?:31(?:(?:([-.\/])(?:0?[13578]|1[02])\1)|(?:([-.\/ ])(?:Jan|JAN|Mar|MAR|May|MAY|Jul|JUL|Aug|AUG|Oct|OCT|Dec|DEC)\2)))|(?:(?:29|30)(?:(?:([-.\/])(?:0?[13-9]|1[0-2])\3)|(?:([-.\/ ])(?:Jan|JAN|Mar|MAR|Apr|APR|May|MAY|Jun|JUN|Jul|JUL|Aug|AUG|Sep|SEP|Oct|OCT|Nov|NOV|Dec|DEC)\4))))(?:(?:1[6-9]|[2-9]\d)?\d{2}))$|^(?:29(?:(?:([-.\/])(?:0?2)\5)|(?:([-.\/ ])(?:Feb|FEB)\6))(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))$|^(?:(?:0?[1-9]|1\d|2[0-8])(?:(?:([-.\/])(?:(?:0?[1-9]|(?:1[0-2])))\7)|(?:([-.\/ ])(?:Jan|JAN|Feb|FEB|Mar|MAR|May|MAY|Jul|JUL|Aug|AUG|Oct|OCT|Dec|DEC)\8))(?:(?:1[6-9]|[2-9]\d)?\d{2}))/
    ];
    for(let i = 0; i < regs.length; i++) {
        const startIndex = parsedString.match(regs[i]);
        if(startIndex && startIndex?.length > 0) {
            return startIndex[0]
        }
    }
    return ''
}
export const removeComma = (data: string) => {
    return data.replace(/\,/g,''); // 1125, but a string, so convert it to number
}
export const getAmount = (parsedString: string) => {
    const newReg = /\d+([\,]\d+)*([\.]\d+)/
    const substringStartIndex = parsedString.search(newReg);
    if (substringStartIndex !== -1) {
        const substring = parsedString.slice(substringStartIndex);
        // find the end index
        const dotIndex = substring.indexOf('.');
        return substring.slice(0, dotIndex + 3)
    } else {
        return ''
    }
}
export const removeFile = (filePath: string) => {
    try {
        fs.unlink(filePath, (err) => {
            if (err) {
                logger.error(err);
            }
        })
    }
    catch(err: any) {
        logger.error(err.message)
    }
}



export const genHash = (stringValue: string): Promise<string> => {
    return new Promise((res, rej) => {
        bcrypt.genSalt(10, function (err: any, salt: string) {
            if (err) {
                rej(err.message)
            }
            bcrypt.hash(stringValue, salt, async (err: any, hash: string) => {
                if (err) {
                    rej(err.message)
                }
                res(hash);
            });
        });
    })
}
export const verifyHash = (password: string, hash: string) => {
    return bcrypt.compare(password, hash);
}
export const castDate = (date: string) => {
    return date.replace(/\//g, '-') + 'T00:00:00Z'
}
export const isHSCurrent = (dataString: string) => {
    return /^([+-]?(?=\.\d|\d)(?:\d+)?(?:\.?\d*))(?:[eE]([+-]?\d+))? [a-zA-Z]+$/.test(dataString);
}
export const getMongooseDateFormat = (data: string) => {
    return castDate(moment(data).format('YYYY/MM/DD'))
}
export const getPercent = (fraction: number, total: number) => {
    return Math.ceil((fraction * total) / 100)
}

export const getMathPercent = (fraction: number, total: number) => {
    return (fraction / total) * 100;
}

export const resizeImages = (image: any, name: string, uniqueId: string, containerWidth: number, containerHeight: number) => {
    const pageWidth = +(containerWidth || 714)
    const pageHeight = +(containerHeight || 1010)
    return new Promise(async (res, rej) => {
        const randomName = name + (Math.round(
            Math.pow(36, 10 + 1) - Math.random() * Math.pow(36, 10)
        )
            .toString(36)
            .slice(1));
        sharp(image)
            .resize(pageWidth, pageHeight)
            .toFile(path.join(__dirname, '../', '../', 'public', 'tmp', randomName + '.png'), async(err, info) => {
                if(err) {
                    // throw new Error(err.message)
                    rej(err.message)
                }
                res({filePath: path.join(__dirname, '../', '../', 'public', 'tmp', randomName + '.png'), uniqueId})
            })
    })
}

export const extractBoundFromImage = (coordinates: any, image: any, name: string, containerWidth: number, containerHeight: number) => {
    const {positionX, positionY, width, height, mappedFieldName} = coordinates;
    const pageWidth = +(containerWidth || 714)
    const pageHeight = +(containerHeight || 1010)
    return new Promise((res, rej) => {
        const randomName = name + (Math.round(
            Math.pow(36, 10 + 1) - Math.random() * Math.pow(36, 10)
        )
            .toString(36)
            .slice(1));
        sharp(image)
            .extract({
                left: getPercent(positionX, pageWidth), top: getPercent(positionY, pageHeight), width: getPercent(width, pageWidth), height: getPercent(height, pageHeight)
            }).toFile(path.join(__dirname, '../', '../', 'public', 'tmp', randomName + '.png'), (err, info) => {
                if(err) {
                    // throw new Error(err.message)
                    rej(err.message)
                }
                logger.info(info);
                res({mappedFieldName, filePath: path.join(__dirname, '../', '../', 'public', 'tmp', randomName + '.png')})
            })
    })
}
export const readFiles = (filePath: string, uniqueId: string) => {
    return new Promise(async (res, rej) => {
        const result = await fs.promises.readFile(filePath)
        res({readFileData: result, uniqueId});
    })
}
export const readImagesUsingGoogleML = (path: string, id: string) => {
    return new Promise(async (res, rej) => {
        const detect = await visionClient.textDetection(path)
        res({value: detect[0].fullTextAnnotation?.text||'NA', key: id})  
    })
}
export const readImagesUsingGoogleMLWithBounds = async (file: Buffer, anchorName = '') => {
    return new Promise(async (res, rej) => {

        const detect: any = await visionClient.textDetection(file)
    
        const extractedTextObjects = detect[0].textAnnotations;
        const targetText = anchorName.replace(/\\n/g, '').toLowerCase()
        const searched: any = extractedTextObjects?.find((val: any) => val.description?.toLowerCase() === targetText);
        if(searched) {
            const [p1, p2, p3, p4] = searched.boundingPoly?.vertices;
            if(!p1 || !p2 || !p3 || !p4) {
                rej('Anchor point not found!')
            }
            const pageWidth = detect[0].fullTextAnnotation?.pages[0].width;
            const pageHeight = detect[0].fullTextAnnotation?.pages[0].height;
            const result = {
                positionX: getMathPercent(p1.x, pageWidth),
                positionY: getMathPercent(p1.y, pageHeight),
                width: getMathPercent((p2.x - p1.x), pageWidth),
                height: getMathPercent((p3.y - p2.y), pageHeight),
                // content: detect
            }
            res(result)
        } else {
            rej('Anchor point not found!');
        }
    })
}

export const generateSearchObject = (keyList: Array<string>, value: string) => {
    const searchRegex = new RegExp(value, 'i');
    const tempObj:{[x:string]: any}[] = [];
    keyList.map((key: string) => {
        tempObj.push({[key]: searchRegex})
    })
    return tempObj;
}

export const idToObjectId = (id:string) => Types.ObjectId(id);