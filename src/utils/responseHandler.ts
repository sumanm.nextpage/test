import { Logger } from "../logger";

export const errorResponse = (err: any, statusCode = 400) => {
    Logger.error(err);
    return {
        data: null,
        error: err.message ? err.message : err,
        message: '',
        status: statusCode,
    };
};

export const successResponse = (data: any) => {
    return {
        data: data,
        error: '',
        message: 'Success',
        status: 200
    };
};