import { existsSync, mkdirSync } from 'fs';
import path from 'path';
import { IFullUrl } from '../interfaces';
export * from './responseHandler'

export const BASE_URl = process.env.BASE_URl || ''

export let fullUrl: IFullUrl = {
    baseUrl: '',
    protocol: '',
    host: '',
    hostname: '',
    pathname: '',
    originalUrl:'',
    query: {}
};

export const setFullUrl = (url:IFullUrl) => {
    fullUrl = url;
    return fullUrl;
}

export const getFullUrl = (key: keyof IFullUrl) => {
    return fullUrl[key]
}

export const testUtils = () => {
    return true;
}

export const createFolder = (dir:string) => {
    try {
        const fullDir = path.join(__dirname, '../', '../') + dir;
        console.log('fullDir', fullDir);
        if (!existsSync(dir)){
            mkdirSync(dir, { recursive: true });
        }
        return dir;
    } catch (err) {
        console.log(err)
        return ""
    }  
}

export const getLocalFilePath = (filePath:string) => {
    return  path.join(__dirname, '../', '../') + filePath
}
