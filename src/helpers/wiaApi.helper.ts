import got from 'got';

const wiaBaseurl = 'https://api.wia.io';

export const gotGetRes = async (endpoint: string, token: string, query: {[x:string]: any}) => {
    const gotRes = await got.get(wiaBaseurl + endpoint, {
        json: true,
        body: {
            ...query
        },
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
    const { statusCode, body } = gotRes;
    return { statusCode, body };
};
