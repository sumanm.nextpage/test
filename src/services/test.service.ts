import { Promise as bluebirdPromise } from 'bluebird';
import { jsonTocsv } from '.';
import { wiaToken } from '../constants';
import { gotGetRes } from '../helpers';

export const getSpaces = async () => {
    const { statusCode, body } = await gotGetRes('/v1/spaces', wiaToken, {});
    return { statusCode, body };
};

export const getDevices = async (spaceId: string) => {
    console.log('spaceId', spaceId);
    const { statusCode, body } = await gotGetRes(`/v1/devices?space.id=${spaceId}`, wiaToken, {});
    return { statusCode, body };
};

export const getEvents = async (deviceId: string) => {
    console.log('deviceId', deviceId);
    const { statusCode, body } = await gotGetRes(`/v1/events?device.id=${deviceId}&name=co2&processed=true`, wiaToken, {});
    return { statusCode, body };
};

const maxMinAvg = (arr: Array<any>) => {
    var max = arr[0].data;
    var min = arr[0].data;
    var sum = 0;
    arr.forEach(function (value) {
        if (value.data > max)
            max = value.data;
        if (value.data < min)
            min = value.data;
        sum += value.data;
    });
    var avg = sum / arr.length;
    return { max, min, avg };
};


export const getCSV = async () => new Promise(async (resolve, reject) => {
    const spacesRes = await getSpaces();
    if (spacesRes.statusCode === 200) {
        if (spacesRes.body.spaces.length) {
            const spaceIds = spacesRes.body.spaces.map((space: any) => space.id);
            bluebirdPromise.map(spaceIds, getDevices, { concurrency: 50 }).then(async (results) => {
                let items: Array<any> = [];
                results.filter((result) => {
                    if (result.statusCode === 200) {
                        items = [...items, ...result.body['devices']];
                    }
                });
                const ids = items.map((item: any) => item.id);
                bluebirdPromise.map(ids, getEvents, { concurrency: 50 }).then(async (results2) => {
                    let items: Array<any> = [];
                    results2.filter((result) => {
                        if (result.statusCode === 200) {
                            items = [...items, ...result.body['events']];
                        }
                    });
                    const fields = [{
                        label: 'Event Name',
                        value: 'name'
                    },
                    {
                        label: 'Event Data',
                        value: 'data'
                    },
                    {
                        label: 'id',
                        value: 'unprocessedEvent.id'
                    }, {
                        label: 'Event timestamp',
                        value:'timestamp'
                    }];
                    const json2csvData = await jsonTocsv([...items], fields, ['unprocessedEvent']);
                    const { max, min, avg  } = maxMinAvg(items)
                    console.log('\n\n\n');
                    console.log(`min:${min},  max:${max}, average:${avg}`)
                    console.log('\n\n\n');
                    resolve({ items: items, ...json2csvData });
                });
            });
        }
    }
});
