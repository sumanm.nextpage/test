import { writeFileSync } from 'fs';
import { Parser, transforms } from 'json2csv';
import { createFolder, fullUrl } from '../utils';
const { unwind } = transforms;

const checkPath = () => {
    return createFolder('public/temp/csv');
};

export const jsonTocsv = async (data: Array<any>, fields: any, paths: Array<string>) => {
    checkPath()
    const fileName = `${'csv'}_${new Date().getTime()}`;
    const fullFilePath = `public/temp/csv/${fileName}.csv`;
    const tempTransforms = [unwind({ paths: paths, blankOut: true })];
    const json2csvParser = new Parser({ fields, transforms: tempTransforms });
    const csv = await json2csvParser.parse(data);
    await writeFileSync(fullFilePath, csv);
    console.log(csv);
    return {
        fileUrl: fullFilePath || '',
        fullFileUrl: `${fullUrl.baseUrl}/${fullFilePath}`,
    }
};