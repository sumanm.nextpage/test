import express, { Application, Request, Response } from "express";
import bodyParser from "body-parser";
// import morgan from "morgan";
import swaggerUi from "swagger-ui-express";
import {Logger, MorganMiddleware } from './logger'
import Router from "./routes";
import cors from 'cors'
import 'dotenv/config'
// connect to mongodb
// import './config/mongodb'
import { BASE_URl, setFullUrl } from './utils';

const PORT = process.env.PORT || 8000;

const app: Application = express();
app.use(cors());
app.use(express.json());
app.use(MorganMiddleware);
app.use(express.static("public"));
app.use('/public', express.static('public'));
app.use(
  "/swagger",
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: "/swagger/swagger.json",
    },
  })
);

// Use body parser to read sent json payloads
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.use(async (_req:Request, _res:Response, _next: () => void) => {
    let protocol =  'http';
    const host = _req.get('host') || ''
    const hostname = _req.hostname;
    const secure = _req.secure;
    if(secure) {
        protocol = 'https'
    }
   setFullUrl({
        baseUrl: BASE_URl ? BASE_URl : `${protocol}://${host}`,
        protocol: protocol,
        host: host,
        hostname: hostname,
        pathname: _req.path,
        originalUrl: _req.originalUrl || '',
        query: _req.query
    })
    _next()
})

app.use('/api', Router);

app.listen(PORT, () => {
    Logger.info(`Server is running on port ${PORT}` );
    Logger.info(`swagger link: localhost:${PORT}/swagger`);
});