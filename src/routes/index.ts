import express, { Request, Response } from 'express';
import StatusController from '../controllers/status.controller';
import TextController from '../controllers/test.controller';

const Route = express.Router();

Route.use('/test/spaces', async (req: Request | any, res: Response) => {
    const controller = new TextController(req, res);
    const response = await controller.spaces();
    return res.send(response);
});

Route.use('/test/devices', async (req: Request | any, res: Response) => {
    const { spaceId } = req.query;
    const controller = new TextController(req, res);
    const response = await controller.devices(spaceId);
    return res.send(response);
});

Route.use('/test/events', async (req: Request | any, res: Response) => {
    const { deviceId } = req.query;
    const controller = new TextController(req, res);
    const response = await controller.events(deviceId);
    return res.send(response);
});

Route.use('/test/csv', async (req: Request | any, res: Response) => {
    const controller = new TextController(req, res);
    const response = await controller.csv();
    return res.send(response);
});

Route.use('/status', async (req: Request | any, res: Response) => {
    const controller = new StatusController(req, res);
    const response = await controller.status();
    return res.send(response);
});





export default Route;