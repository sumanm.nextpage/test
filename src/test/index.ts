const expectMocha = require("chai").expect;
const request = require("supertest");
const app = require("../index.ts");
const appUrl =  'http://localhost:8000'

describe("/api/test/spaces", () => {
  describe("GET /", () => {
    it("should return all spaces", async () => {
      const res = await request(appUrl).get("/api/test/spaces");
      expectMocha(res.status).to.equal(200);
    });
  });
});

describe("/api/test/devices", () => {
  describe("GET /", () => {
    it("should return a device if valid spaceId is passed", async () =>  {
      const validSpaceId = 'spc_FrdEbKUt';
      const res = await request(appUrl).get("/api/test/devices?spaceId=" + validSpaceId);
      expectMocha(res.status).to.equal(200);
    });
  });
});

describe("/api/test/events", () => {
  describe("GET /", () => {
    it("should return events if valid deviceId is passed", async () =>  {
      const validDeviceId = 'dev_7oaz0g3g';
      const res = await request(appUrl).get("/api/test/events?deviceId=" + validDeviceId);
      expectMocha(res.status).to.equal(200);
    });
  });
});