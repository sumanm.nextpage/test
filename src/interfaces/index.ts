export interface IResponse {
    message: string;
    error: any;
    data: any;
    status: number;
}

interface IObj {
    [x: string]: any;
}

export interface IFullUrl {
    baseUrl: string;
    protocol: string;
    host: string;
    hostname: string;
    pathname: string;
    originalUrl: string;
    query: IObj;
}